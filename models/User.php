<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface // añadir esa implementación
{
    
    public static function tableName() // se ñaden usuario manualmente
    {
        return 'usuarios';
    }
    
    public function rules()
    {
        return [
        ];
    }
    
    public static function findIdentity($id) // añadir estas propiedades
    {
        return static::findOne(['id' => $id]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['usuario' => $username]);
    }
    

     /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    /*public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }*/
    
    
    public function validatePassword($password) // se varia se explicará
    {
        return $this->password === $password; // ==== es como un as
    }
    
     /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }
    
    public static function findIdentityByAccessToken($token, $type = null) // se añade
    {
        return null;
    }
    
}
